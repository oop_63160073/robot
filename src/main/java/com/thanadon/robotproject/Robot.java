/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.robotproject;

/**
 *
 * @author Acer
 */
public class Robot {
    private int x;
    private int y;
    private int bx;
    private int by;
    private int N;
    private char lastDirection = 'N';
    
    public Robot(int x, int y, int bx, int by, int N){
        this.x = x;
        this.y = y;
        this.bx = bx;
        this.by = by;
        this.N = N;
    }
    public boolean inMap(int x, int y){
        if((x < 0 || x > N) || (y < 0 || y > N))
            return false;
        return true;
    }
    
    public boolean walk(char direction){
        switch(direction) {
            case 'N':
                if(!inMap(x, y-1)){
                    CantMove();
                    return false;
                }
                y = y-1;
                break;
            case 'S':
                if(!inMap(x, y+1)){
                    CantMove();
                    return false;
                }
                y = y+1;
                break;
            case 'E':
                if(!inMap(x+1, y)){
                    CantMove();
                    return false;
                }
                x = x+1;
                break;
            case 'W':
                if(!inMap(x-1, y)){
                    CantMove();
                    return false;
                }
                x = x-1;
                break;            
        }
        lastDirection = direction;
        if(isBomb(x, y)){
            System.out.println("Bomb found!!!");
        }
        return true;
    }
    
    public boolean walk(char direction, int step){
        for(int i = 1; i <= step; i++){
            if(!Robot.this.walk(direction)){
                return false;
            }
        }
        return true;
    }
    
    public boolean walk(){
        return Robot.this.walk(lastDirection);
    }
    
    public boolean walk(int step){
        return Robot.this.walk(lastDirection, step);
    }
    
    public void CantMove(){
        System.out.println("I can't move!!!");
    }
    
    public boolean isBomb(int x, int y){
        if(x == bx && y == by){
            return true;
        }
        return false;
    }
    
    
    public String toString(){
        return "Robot(" + this.x +", " + this.y + ") - (" + Math.abs(x-bx) + ", " + Math.abs(y-by) + ")";
    }
        
    
}
